project_path = ARGV[0].strip

git_modified = %x{
  cd #{project_path}
  git status
}

strings = git_modified.split(/\n/).collect{ |element| element.split(/\s+/) }

modified_ruby_files = strings.flatten.collect do |string|
  string if string.match('.rb')
end.compact.uniq

puts 'New & Modified Ruby files are:'

modified_ruby_files.each_with_index do |file, index|
  puts "#{index + 1}. #{file}"
end

puts 'Applying safe autocorrect...'

for file in modified_ruby_files
  %x{
    cd #{project_path}
    rubocop #{file} --safe-auto-correct
  }
end

puts 'Done.'
puts 'Thank you for using me.'
