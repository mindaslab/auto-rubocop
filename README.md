# Auto Rubocop

![](https://sc01.alicdn.com/kf/UTB8JL62gbnJXKJkSahGq6xhzFXaX/102803965/UTB8JL62gbnJXKJkSahGq6xhzFXaX.jpg)

Automatically applies rubocop safe auto correct to modified ruby files in your project, thus increases your code Karma.

## Requirements

* Your project must use git
* Your project should have rubocop in its gem file (bundle it)

## Usage

Clone this project into any place.

Once you have modified files, after `git add` and before `git commit` do this

`$ ruby auto_rubocop <your project path>`

The above command finds the modified ruby files and applies rubocop safe autocorrect to it.

## Author

Karthikeyan A K, mindaslab@protonmail.com, +91 8428050777

## Contributions

Contributions and ideas are welcome
